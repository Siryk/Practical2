﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practical_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter array:");
            string s = "";
            s = Console.ReadLine();
            var splitString = s.Split(' ');

            int[] convertedItems = null;
            try
            {
                convertedItems = Array.ConvertAll<string, int>(splitString, int.Parse);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            Console.Clear();

            Console.WriteLine("Enter base number:");
            int numb = Int32.Parse(Console.ReadLine());

            FindPow(convertedItems, numb);

            Console.ReadKey();
        }
        public static void FindPow(int[] arr, int numb)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                int j = 0;

                while (true)
                {
                    if(Pow(numb, j).Equals(arr[i])){
                        Console.WriteLine("Good");
                        break;
                    }else if(Pow(numb,j) > arr[i])
                    {
                        Console.WriteLine("Bad");
                        break;
                    }
                    j++;
                }             
            }
        }

        public static int Pow(int basevalue, int exponentvalue)
        {
            if (exponentvalue == 0)
            {
                return 1;
            }
            if (exponentvalue == 1)
            {
                return basevalue;
            }
            return basevalue * Pow(basevalue, exponentvalue - 1);
        }
    }
}

